<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">    
    <title>OFERTA</title>
    <link rel="stylesheet" href="aos-master/dist/aos.css">
    <link href='styles/styles.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="aos-master/dist/aos.js"></script>
    <script src="scripts/scripts.js"></script>
</head>
<body onload="highlightNavElement('nav-offer');">
    <div class="container-fluid w-100 p-0">
        <?php
            include('top.php');
        ?>
        <div class="content">
            <div class="offer-content">
                <div class="offer-top"></div>
                <div class="content-header">
                    Oferta
                </div>
                <!-- https://michalsnik.github.io/aos/ -->
                <!-- FADEIN ON SCROLL LIBRARY -->
                <div class="offer-sections">
                    <div class="container">
                        <div class="row">
                            <div class="p-3 col-sm-12 col-md-6 order-first">
                                <div class="offer-section">
                                    <div class="offer-section-img" data-aos="fade-right" data-aos-offset="50" data-aos-once="true">
                                        <img src="images/offer/offer-wypad.jpg" />
                                    </div>
                                </div>
                            </div>
                            <div class="p-3 col-sm-12 col-md-6">
                                <div class="offer-section">
                                    <div class="offer-section-text" data-aos="fade-left" data-aos-offset="50" data-aos-once="true">
                                        <span class="offer-section-text-header">
                                            NAUKA JAZDY
                                        </span>
                                        <span class="offer-section-text-description">
                                            Pomagamy stawiać pierwsze kroki na lodzie.
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="p-3 col-sm-12 col-md-6 order-first order-md-last">
                                <div class="offer-section">
                                    <div class="offer-section-img" data-aos="fade-left" data-aos-offset="50" data-aos-once="true">
                                        <img src="images/offer/small-offer-wheel.jpg" />
                                    </div>
                                </div>
                            </div>
                            <div class="p-3 col-sm-12 col-md-6">
                                <div class="offer-section">
                                    <div class="offer-section-text" data-aos="fade-right" data-aos-offset="50" data-aos-once="true">
                                        <div class="offer-section-text-header">
                                            GRUPY SYNCHRONICZNE
                                        </div>
                                        <div class="offer-section-text-description">
                                            Tworzymy drużyny, które startują na zawodach krajowych i zagranicznych.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="p-3 col-sm-12 col-md-6 order-first">
                                <div class="offer-section">
                                    <div class="offer-section-img" data-aos="fade-right" data-aos-offset="50" data-aos-once="true">
                                        <img src="images/offer/offer-puenta.png" />
                                    </div>
                                </div>
                            </div>
                            <div class="p-3 col-sm-12 col-md-6">
                                <div class="offer-section">
                                    <div class="offer-section-text" data-aos="fade-left" data-aos-offset="50" data-aos-once="true">
                                        <div class="offer-section-text-header">
                                            BALET
                                        </div>
                                        <div class="offer-section-text-description">
                                            Zajęcia są prowadzone przez doświadczonego pedagoga tańca klasycznego.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="p-3 col-sm-12 col-md-6 order-first order-md-last">
                                <div class="offer-section">
                                    <div class="offer-section-img" data-aos="fade-left" data-aos-offset="50" data-aos-once="true">
                                        <img src="images/offer/small-offer-gym.jpg" />
                                    </div>
                                </div>
                            </div>
                            <div class="p-3 col-sm-12 col-md-6">
                                <div class="offer-section">
                                    <div class="offer-section-text" data-aos="fade-right"  data-aos-once="true">
                                        <div class="offer-section-text-header">
                                            TRENINGI OFF-ICE
                                        </div>
                                        <div class="offer-section-text-description">
                                            Zajęcia ogólnorozwojowe, nauka choreografii, zajęcia imitacyjne.
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include('footer.php');
    ?>
    <script src="scripts/aos.js"></script>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KONTAKT</title>
    <link href='styles/styles.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> -->
    <script src="scripts/scripts.js"></script>
    <script src="https://kit.fontawesome.com/76a93ee453.js" crossorigin="anonymous"></script>
</head>
<body onload="highlightNavElement('nav-contact');">
    <div class="container-fluid w-100 p-0">
        <?php
            include('top.php');
        ?>
        <div class="content">
            <div class="contact-content">
                <div class="contact-top"></div>
                <div class="content-header">
                    Kontakt
                </div>
                <!-- <div class="contact-facebook">
                <div id="fb-root"></div> -->
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v13.0" nonce="uLjnL1EO"></script>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="fb-page" 
                                data-href="https://www.facebook.com/iceparadisepoland"
                                data-width="380" 
                                data-hide-cover="false"
                                data-show-facepile="false">
                            </div>
                        </div>
                        <div class="p-3 col-sm-12 col-md-6">
                            <div class="instagram-section">
                                <div class="row">
                                    <a href="https://www.instagram.com/iceparadiseclub/" target="_blank">
                                        <div class="p-3 col-4 instagram-section-element">
                                            <i class="fa-brands fa-instagram instagram"></i>
                                            <div class="instagram-subtext">
                                                <span>Ice Paradise</span>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.instagram.com/icecrystalsteam/" target="_blank">
                                        <div class="p-3 col-4 instagram-section-element">
                                            <i class="fa-brands fa-instagram instagram"></i>
                                            <div class="instagram-subtext">
                                                <span>Ice Crystals<br/>Mixed Age</span>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="https://www.instagram.com/icecrystalskidsteam/" target="_blank">
                                        <div class="p-3 col-4 instagram-section-element">
                                            <i class="fa-brands fa-instagram instagram"></i>
                                            <div class="instagram-subtext">
                                                <span>Ice Crystals<br/>Pre-Juvenile</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row contact-last-line">
                        <div class="p-3 col-sm-12 col-md-6">
                            <div class="contact-contact">
                                <span class="contact-contact-header">
                                    Trójmiejski Klub Łyżwiarski Ice Paradise<br/>
                                </span>
                                <div class="contact-contact-content">
                                    <a class="contact-contact-link" href="tel:+48515523254">
                                        <i class="fa-solid fa-phone"></i>
                                        515-523-254
                                    </a><br/>
                                    <a class="contact-contact-link" href="mailto:kontakt@iceparadise.pl">
                                        <i class="fa-solid fa-envelope"></i>
                                        kontakt@iceparadise.pl
                                    </a>
                                </div>
                                <br/>
                                <span class="contact-contact-address-header">
                                    Gdzie trenujemy:<br/>
                                </span>
                                <div class="contact-contact-address-content">
                                    Hala Olivia<br/>
                                    al. Grunwaldzka 470<br/>
                                    80-309 Gdańsk
                                </div>
                            </div>
                        </div>
                        <div class="p-3 col-sm-12 col-md-6">
                            <div class="contact-image">
                                <img src='/iceparadise/images/contact/contact-image.jpg' />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include('footer.php');
    ?>
</body>
</html>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="p-3 col-12 col-md-6">
                <div class="pge-section">
                    <div class="pge-section-header">
                        Sponsor główny
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="pge-img">
                                <a href="https://www.gkpge.pl/" target="_blank">
                                    <img src="/iceparadise/images/footer/pge.png" />
                                </a>
                            </div>
                        </div>
                            <div class="col-6">
                                <div class="pge-junior-img">
                                    <img src="/iceparadise/images/footer/pge-junior.png" />
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="p-3 col-12 col-md-6">
                <div class="gks-section">
                    <div class="gks-section-header">
                        Partner
                    </div>
                    <div class="gks-img">
                        <a href="http://www.stoczniowiec.org.pl/" target="_blank">
                            <img src="/iceparadise/images/footer/stoczniowiec.png" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top-navbar">
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-blue">
    <div class="container-fluid">
        <div class="navbar-brand">
            <a href="/iceparadise/">
                <div class="top-logos">
                    <div class="logo-ip">
                        <img src="/iceparadise/images/ip_logo_bg.png"/>
                    </div>
                    <div class="logo-ic">
                        <img src="/iceparadise/images/icecrystals2.png"/>
                    </div>
                </div>
            </a>
        </div>
        <button class="navbar-toggler navbar-toggler-custom" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
            <li class="nav-item">
                <a class="nav-link nav-link-color nav-about" aria-current="page" href="/iceparadise/">
                    O nas
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle nav-link-color nav-teams" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Nasze drużyny
                </a>
                <ul class="dropdown-menu slideIn" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item dropdown-item-custom" href="/iceparadise/druzyny/mixed-age">Mixed Age</a></li>
                    <li><a class="dropdown-item dropdown-item-custom" href="/iceparadise/druzyny/pre-juvenile">Pre-Juvenile</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-color nav-offer" aria-current="page" href="/iceparadise/oferta">
                    Oferta
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-color nav-gallery" aria-current="page" href="/iceparadise/galeria">
                    Galeria
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-color nav-news" aria-current="page" href="/iceparadise/aktualnosci">
                    Aktualności
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-color nav-contact" aria-current="page" href="/iceparadise/kontakt">
                    Kontakt
                </a>
            </li>
        </ul>
        </div>
    </div>
    </nav>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
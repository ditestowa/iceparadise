function highlightNavElement(navName) {
    let element = document.getElementsByClassName(navName)[0];
    element.classList.add('nav-link-color-highlight');

    let header = document.getElementsByClassName('content-header')[0];
    if (header) {
        header.classList.add('max-opacity');
    }

    
    let aboutHeaderImg = document.getElementsByClassName('about-content-header-img')[0];
    if (aboutHeaderImg) {
        console.log('about!');
        console.log(aboutHeaderImg);
        aboutHeaderImg.classList.add('show-logo');
        
        let scrollDown = document.querySelector('.scroll-down');
        
        if (scrollDown) {
            aboutHeaderImg.addEventListener('transitionend', () => {
                scrollDown.classList.add('show-icon');
            })
        }
    }
}

function showLogo(className) {
    let element = document.getElementsByClassName(className)[0];
    element.classList.add('max-opacity');
}
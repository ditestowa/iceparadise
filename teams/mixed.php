<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NASZE DRUŻYNY</title>
    <link href='../styles/styles.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="../scripts/scripts.js"></script>
</head>
<body onload="highlightNavElement('nav-teams');">
    <div class="container-fluid w-100 p-0">
        <?php
            include('../top.php');
        ?>
        <div class="content">
            <div class="teams-content">
                <div class="teams-top"></div>
                <div class="team team-mixed">
                    <div class="team-name">
                        Ice Crystals Mixed Age
                    </div>
                    <div class="team-image">
                        <img src='/iceparadise/images/teams/team-mixed-age.jpg' />
                        <div class="team-image-description">
                            Od lewej: Ola, Paulina, Mila, Klaudia, Julia, Ala, Nela, Magda, Emilia, Aga, Karolina, Asia, Emma
                        </div>
                    </div>
                    <div class="team-description">
                        <p>
                            W grupie Ice Crystals Mixed Age zrzeszamy łyżwiarki w wieku od 12 do 40 lat. Serdecznie 
                            zapraszamy zarówno dziewczęta z doświadczeniem w jeździe synchronicznej, zawodniczki z kategorii solistycznych i parowych, 
                            jak i osoby pragnące dopiero zacząć przygodę z łyżwiarstwem.
                        </p>
                        <p>
                            Trenerzy: Klaudia Kmiecik-Stencel, Anna Madej
                        </p>
                        <p>
                            Muzyka: <br/>
                            Sezon 2021/2022 - Adele - Rolling In The Deep / Rumour Has It <br/>
                            Sezon 2020/2021 - Imelda May - Big Bad Handsome Man
                        </p>
                    </div>
                    <div class="team-more-images">
                        <div class="container">
                            <div class="row">
                                <div class="p-3 col-sm-12 col-md-6 order-last order-md-first">
                                    <div class="team-more-image">
                                        <img src='/iceparadise/images/teams/small-team-mixed-age-more-1.jpg' />
                                    </div>
                                </div>
                                <div class="p-3 col-sm-12 col-md-6">
                                    <div class="team-more-image">
                                        <img src='/iceparadise/images/teams/small-team-mixed-age-more-2.jpg' />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include('../footer.php');
    ?>
</body>
</html>
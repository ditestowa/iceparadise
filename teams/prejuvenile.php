<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NASZE DRUŻYNY</title>
    <link href='../styles/styles.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="../scripts/scripts.js"></script>
</head>
<body onload="highlightNavElement('nav-teams');">
    <div class="container-fluid w-100 p-0">
        <?php
            include('../top.php');
        ?>
        <div class="content">
            <div class="teams-content">
                <div class="teams-top"></div>
                <div class="team team-mixed">
                    <div class="team-name">
                        Ice Crystals Pre-Juvenile
                    </div>
                    <div class="team-image">
                        <img src='/iceparadise/images/teams/team-pre-juvenile-cut.jpg' />
                        <div class="team-image-description">
                            Od lewej: Idalia, Jagoda, Emma, Olimpia, Ola, Marysia, Maja, Hania, Maja, Sandra, Tosia, Marta, Józia, Karolina, Rozalia, Eliza, Lena
                        </div>
                    </div>
                    <div class="team-description">
                        <p>
                            W grupie Ice Crystals Pre-Juvenile trenują dziewczynki w wieku od 6 do 12 lat. 
                            Wiele z nich stawiało z nami swoje swoje pierwsze kroki na lodzie. Treningi młodszej grupy charakteryzują się 
                            pracą poprzez zabawę, ciągłym uśmiechem, wesołą i przyjacielską atmosferą.
                        </p>
                        <p>
                            Trenerzy: Anna Madej, Klaudia Kmiecik-Stencel
                        </p>
                        <p>
                            Muzyka: <br/>
                            Sezon 2021/2022 - Rachel Platten - Fight Song <br/>
                            Sezon 2020/2021 - Natalia Nykiel - Pół kroku stąd (z filmu "Vaiana: Skarb oceanu")
                        </p>
                    </div>

                    <div class="team-more-images">
                        <div class="container">
                            <div class="row">
                                <div class="p-3 col-6 col-md-3 order-first">
                                    <div class="team-more-image">
                                        <img src='/iceparadise/images/teams/team-pre-juvenile-more-1.jpg' />
                                    </div>
                                </div>
                                <div class="p-3 col-12 col-md-6 order-last order-md-2">
                                    <div class="team-more-image">
                                        <img src='/iceparadise/images/teams/small-team-pre-juvenile-more-2.jpg' />
                                    </div>
                                </div>
                                <div class="p-3 col-6 col-md-3 order-2 order-md-last">
                                    <div class="team-more-image">
                                        <img src='/iceparadise/images/teams/team-pre-juvenile-more-3.jpg' />
                                    </div>
                                </div>
                                <!-- LAZY LOAD NA ZDJĘCIA -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include('../footer.php');
    ?>
</body>
</html>
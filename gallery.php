<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GALERIA</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href='styles/styles.css' rel='stylesheet' type='text/css' />
    <link href='styles/lightbox.min.css' rel='stylesheet' type='text/css' />
    <script src="scripts/scripts.js"></script>
    <script src="scripts/lightbox-plus-jquery.min.js"></script>
</head>
<body onload="highlightNavElement('nav-gallery');">
    <div class="container-fluid w-100 p-0">
        <?php
            include('top.php');
        ?>
        <div class="content">
            <div class="gallery-content">
                <div class="gallery-top"></div>
                <div class="content-header">
                    Galeria
                </div>
                <!-- Wyłączyć hover na image na mobilce -->
                <div class="container">
                    <div class="row">
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/1.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-1.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/2.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-2.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/3.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-3.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/4.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-4.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/5.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-5.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/6.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-6.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/7.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-7.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/8.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-8.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/9.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-9.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/10.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-10.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/11.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-11.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/12.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-12.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/13.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-13.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/14.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-14.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/15.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-15.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/16.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-16.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/17.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-17.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/18.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-18.jpg' />
                                </a>
                            </div>
                        </div>
                        <div class="p-3 col-12 col-sm-6">
                            <div class="gallery-image">
                                <a data-lightbox="mygallery" href='/iceparadise/images/gallery/19.jpg'>
                                    <img class="lazy img-fluid" data-src='/iceparadise/images/gallery/small-19.jpg' />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include('footer.php');
    ?>
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.5.0/dist/lazyload.min.js"></script>
    <script src="scripts/lazyload.js"></script>
    <script>
        lazyLoadInstance.update();

        lightbox.option({
            "showImageNumberLabel": false,
            "wrapAround": true,
            "fadeDuration": 300,
            "alwaysShowNavOnTouchDevices": true
        });
    </script>
</body>
</html>
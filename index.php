<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ICE PARADISE</title>
    <link href='styles/styles.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="scripts/scripts.js"></script>
</head>
<body onload="showLogo('main-content');">
    <div class="container">
        <?php
            include('top.php');
        ?>
       <div class="content">
        <div class="main-content">
            <img src="images/iceparadise.jpg" />
        </div>
       </div>
    </div>
</body>
</html>
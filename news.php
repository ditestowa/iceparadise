<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AKTUALNOŚCI</title>
    <link href='styles/styles.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="scripts/scripts.js"></script>
</head>
<body onload="highlightNavElement('nav-news');">
    <div class="container-fluid w-100 p-0">
        <?php
            include('top.php');
        ?>
        <div class="content">
            <div class="news-content">
                <div class="news-top"></div>
                <div class="content-header">
                    Aktualności
                </div>
                <div class="news-sections">
                    <div class="container">
                        <div class="row">
                            <div class="p-3 col-sm-12 col-md-8">
                                <div class="news-section">
                                    <div class="news-section-text">
                                        <span class="news-section-text-header">
                                            2022-03-22
                                        </span>
                                        <span class="news-section-text-description">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ultricies luctus ex eget viverra. Maecenas eleifend libero ac venenatis bibendum. Suspendisse potenti. Nam erat neque, laoreet eget posuere in, consequat quis dolor. Phasellus tempus lectus ante, in faucibus purus suscipit in. Fusce maximus, felis in cursus vestibulum, nunc nisi ultrices metus, vel commodo tellus quam in arcu. Phasellus sit amet nulla eget purus dapibus rhoncus. Duis sagittis eget mi ut rhoncus. Quisque tincidunt nisi at turpis pulvinar mollis. Quisque in tincidunt turpis. Nam erat dolor, auctor accumsan volutpat in, feugiat id odio. Vivamus sollicitudin dui quis sapien lobortis cursus. Donec efficitur odio eu purus aliquam, eget condimentum erat rhoncus. Maecenas fringilla, ex id sagittis iaculis, felis erat elementum lorem, vitae faucibus neque dui quis tellus. Vivamus sagittis nec nibh et iaculis.
                                            </p>
                                            <p>
                                                Vivamus eleifend suscipit rhoncus. Donec ac metus ac tortor bibendum sollicitudin. Vestibulum arcu tellus, ornare sed dapibus et, maximus at nunc. Etiam dictum id nisl eu dictum. Sed non suscipit nisl. Nunc non risus eu erat congue interdum. Aenean mauris enim, mollis ac finibus at, iaculis eleifend tortor. Proin eget leo faucibus, elementum elit ut, suscipit tortor. Maecenas vehicula vel dolor eget bibendum. Sed sit amet tortor quis risus posuere porttitor.
                                            </p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="p-3 col-sm-12 col-md-4">
                                <div class="news-section">
                                    <div class="news-section-img">
                                        <img src="images/offer/small-offer-gym.jpg" />
                                    </div>
                                </div>
                            </div>
                            <div class="p-3 col-sm-12 col-md-8">
                                <div class="news-section">
                                    <div class="news-section-text">
                                        <span class="news-section-text-header">
                                            2021-12-14
                                        </span>
                                        <span class="news-section-text-description">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ultricies luctus ex eget viverra. Maecenas eleifend libero ac venenatis bibendum. Suspendisse potenti. Nam erat neque, laoreet eget posuere in, consequat quis dolor. Phasellus tempus lectus ante, in faucibus purus suscipit in. Fusce maximus, felis in cursus vestibulum, nunc nisi ultrices metus, vel commodo tellus quam in arcu. Phasellus sit amet nulla eget purus dapibus rhoncus. Duis sagittis eget mi ut rhoncus. Quisque tincidunt nisi at turpis pulvinar mollis. Quisque in tincidunt turpis. Nam erat dolor, auctor accumsan volutpat in, feugiat id odio. Vivamus sollicitudin dui quis sapien lobortis cursus. Donec efficitur odio eu purus aliquam, eget condimentum erat rhoncus. Maecenas fringilla, ex id sagittis iaculis, felis erat elementum lorem, vitae faucibus neque dui quis tellus. Vivamus sagittis nec nibh et iaculis.
                                            </p>
                                            <p>
                                                Vivamus eleifend suscipit rhoncus. Donec ac metus ac tortor bibendum sollicitudin. Vestibulum arcu tellus, ornare sed dapibus et, maximus at nunc. Etiam dictum id nisl eu dictum. Sed non suscipit nisl. Nunc non risus eu erat congue interdum. Aenean mauris enim, mollis ac finibus at, iaculis eleifend tortor. Proin eget leo faucibus, elementum elit ut, suscipit tortor. Maecenas vehicula vel dolor eget bibendum. Sed sit amet tortor quis risus posuere porttitor.
                                            </p>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="p-3 col-sm-12 col-md-4">
                                <div class="news-section">
                                    <div class="news-section-img">
                                        <img src="images/offer/small-offer-wheel.jpg" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include('footer.php');
    ?>
</body>
</html>
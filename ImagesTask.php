<?php

class ImagesTask
{
    // public const GALLERY_PATH = 'images/gallery';
    // public const GALLERY_PATH = 'images/about';
    // public const GALLERY_PATH = 'images/teams';
    public const GALLERY_PATH = 'images/tmp';

    public static function scaleGalleryImagesAction(): void
    {
        // wywolanie w terminalu:
        // być może wcześniej potrzebne: php > require 'ImagesTask.php';
        // php -r 'include "ImagesTask.php"; ImagesTask::scaleGalleryImagesAction();'

        $path    = self::GALLERY_PATH;
        $images = array_diff(scandir($path), array('.', '..'));

        foreach ($images as $imageName) {
            $newImageName = 'small-' . $imageName;
            $imagePath = self::GALLERY_PATH . '/' . $imageName;
            $newImagePath = self::GALLERY_PATH . '/' . $newImageName;

            header('Content-Type: image/jpeg');

            list($width, $height) = getimagesize($imagePath);

            if ($width >= 3000) {
                $percent = 0.25;
            } else {
                $percent = 0.5;
            }

            $new_width = $width * $percent;
            $new_height = $height * $percent;

            $image_p = imagecreatetruecolor($new_width, $new_height);
            $image = imagecreatefromjpeg($imagePath);

            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        
            imagejpeg($image_p, $newImagePath, 100);
        }
    }
}

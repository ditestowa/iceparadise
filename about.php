<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>O NAS</title>
    <link href='styles/styles.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="scripts/scripts.js"></script>
    <script src="https://kit.fontawesome.com/76a93ee453.js" crossorigin="anonymous"></script>
</head>
<body onload="highlightNavElement('nav-about');">
    <div class="container-fluid w-100 p-0">
        <?php
            include('top.php');
        ?>
        <div class="content">
            <div class="about-content">
                <div class="about-content-top">
                    <div class="about-top"></div>
                    <div class="about-content-header">
                        <img class="about-content-header-img" src="images/ip_logo_bg.png" />
                        <div class="scroll-down">
                            <a href="#about-text">
                                <i class="fa-solid fa-angles-down scroll-down-icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="about-text">
                    <p>
                        <span class="about-text-bold">Trójmiejski Klub Łyżwiarski Ice Paradise</span> został założony z miłości do łyżwiarstwa synchronicznego w kwietniu 2018 roku przez Annę Madej, Klaudię Kmiecik-Stencel oraz Aleksandrę Rękawek.
                    </p>
                    <p>
                        Początkowo klub zrzeszał 8 członków. Pierwszy raz <span class="about-text-bold">TKŁ Ice Paradise</span> zaprezentował się szerszej publiczności w grudniu 2018, kiedy w Mistrzostwach Polski w Łyżwiarstwie Synchronicznym w kategorii Mixed Age wystartowała drużyna Frozen Fenix. 8 zawodniczek zyskało sympatię publiczności wykonując swój pierwszy program do piosenki Celine Dion: „Alive”.
                    </p>
                    <p>
                        Od sezonu 2020/2021 klub regularnie wystawia na zawody krajowe dwa zespoły Ice Crystals: w kategorii Pre-Juvenile oraz w kategorii Mixed Age. W kwietniu 2022 dziewczęta po raz pierwszy zaprezentowały swoje programy poza granicami Polski, biorąc udział w zawodach 27th Jihlavsky Jezek.
                    </p>
                    <p>
                        W tej chwili w barwach <span class="about-text-bold">Ice Paradise</span> trenuje prawie 40 zawodniczek w wieku od 6 do 40 lat. Naszym głównym celem jest rozwój i propagowanie tej wyjątkowej dyscypliny, zarówno na poziomie amatorskim, jak i wyczynowym. Stawiamy na zapewnienie profesjonalnych warunków treningowych i jak najlepsze przygotowanie podopiecznych do zawodów krajowych oraz międzynarodowych.
                    </p>
                    <p>
                        <span class="about-text-bold">Ice Paradise</span> to miejsce, gdzie poprzez pasję, przyjaźń i wspólne zaangażowanie ze wspaniałymi zawodnikami, wyjątkowymi rodzicami oraz trenerami dążymy do realizacji marzeń.
                    </p>
                </div>
                <div class="about-image">
                    <img src="images/about/small-team-about.jpg" />
                </div>
        </div>
       </div>
    </div>
    <?php
        include('footer.php');
    ?>
</body>
</html>